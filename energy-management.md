# Energy Management

## 1. What are the activities you do that make you relax - Calm quadrant?
- Listening to Music
- Meditation
- Nature Walks
- Practicing yoga

## 2. When do you find getting into the Stress quadrant?
- Tight Deadlines
- Workload Overload:
- Health Issues
- Major Life Changes

## 3. How do you understand if you are in the Excitement quadrant?
- Positive Interactions
- Increased Energy Levels
- Increased Creativity
- Elevated Mood

## 4. Paraphrase the Sleep is your Superpower video in your own words in brief. Only the points, no explanation.
- Sleep improves learning, memory consolidation, and problem-solving skills.
- Sleep is essential for overall health and well-being.
- Lack of sleep affects physical, mental, and emotional health.
- Prioritize sleep for optimal health, productivity, and happiness.

## 5. What are some ideas that you can implement to sleep better?
- Create a Comfortable Sleep Environment.
- Create a Relaxing Bedtime Routine.
- Before going to bed, stay away from phones and TV.
- Establish a relaxing evening activity.
Make your bedroom dark.

## 6. Paraphrase the video - Brain Changing Benefits of Exercise. Minimum 5 points, only the points.
- Regular exercise has a positive impact on brain health and cognitive function.
- Physical activity helps your brain get smarter.
- When you move your body, your brain changes for the positive.
- The advantages of physical activity in reshaping brain functions.

## 7. What are some steps you can take to exercise more?
- Set Realistic Goals
- Schedule Exercise Into Your Day
- Find Activities You Enjoy

Regular exercise is essential for maintaining good health. It helps strengthen the body and improves overall well-being.