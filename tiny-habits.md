# Tiny Habits

## 1. In this video, what was the most interesting story or idea for you?

The most intriguing idea from the video was the Think of it like planting a seed for a new habit. Starting small is like giving that seed just the right amount of water and sunlight to help it grow. Eventually, with consistent care and attention, it will blossom into a strong and healthy habit.

## 2. How can you use B = MAP to make making new habits easier? What are M, A and P.

Using the B = MAP formula can make forming new habits easier.
-  **Make it Easy** - Start with small, easy steps for your new habit. Keep it simple so you're more likely to stick with it.
-  **Make it Enjoyable** - Find ways to make your new habit fun or rewarding. When you enjoy it, you're more likely to keep doing it.
-  **Make it Meaningful** - Connect your new habit to something important to you. When it has purpose, it becomes more meaningful and motivating.


## 3. Why it is important to "Shine" or Celebrate after each successful completion of habit? (This is the most important concept in today's topic. Whatever you celebrate becomes a habit)


Celebrating after each successful completion of a habit is like giving yourself a high-five or a pat on the back. It reinforces the habit and makes you feel good, motivating you to keep it up. Plus, the more you celebrate, the more the habit sticks!

## 4. In this video, what was the most interesting story or idea for you?

The idea that small, consistent improvements can lead to significant success over time was captivating. It emphasizes the importance of incremental progress.

## 5. What is the book's perspective about Identity?

The book likely talks about how the things we do every day shape who we are. It's like building blocks - each habit adds up to create our identity. By choosing our habits wisely, we can shape our identity in a positive way.

## 6. Write about the book's perspective on how to make a habit easier to do?


The book likely offers several strategies to make habits easier to do, emphasizing the importance of simplicity, enjoyment, and purpose. 

- **Start Small:** Begin with tiny steps that are easy to accomplish, gradually increasing the difficulty over time.
- **Make it Enjoyable:** Find ways to make the habit fun or rewarding, so you're more likely to stick with it.


## 7. Write about the book's perspective on how to make a habit harder to do?


The book may suggest several strategies to make a habit harder to do, focusing on increasing awareness, introducing obstacles, and altering the environment.

- **Introduce Obstacles:** Make it harder to perform the habit by adding obstacles or barriers that require extra effort or time.
- **Change the Environment:** Alter your surroundings to make it less convenient or appealing to engage in the habit, reducing its accessibility and increasing resistance.

## 8. Pick one habit that you would like to do more of? What are the steps that you can take to make it make the cue obvious or the habit more attractive or easy and or response satisfying?

- Set a specific time and place for meditation.
- Reflect on the enjoyment and relaxation I experience while reading before bed.
- Allocate 10 minutes every morning upon waking.

## 9. Pick one habit that you would like to eliminate or do less of? What are the steps that you can take to make it make the cue invisible or the process unattractive or hard or the response unsatisfying?

- Remove social media apps from my phone's home screen.
- Set time limits for social media use and reflect on how it affects my mood and productivity.
- Replace social media scrolling with more fulfilling activities, like reading or spending time with friends. 