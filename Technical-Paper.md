# FIREWALLS

## Introduction

A **Firewall** is a Network Security System that monitors and controls the incoming and outgoing network traffic based on predetermined security rules, often termed as **Access Control Lists (ACL)**. It acts as a safety barrier between a secured and trusted Private Network and an untrusted Public Network, such as the internet, where malicious viruses and hackers attempt to access important and personal data.

## Working of Firewalls

Firewalls typically operate at the junction or gateway between two networks, such as Private and Public Networks. They commonly function at layer 3 and layer 4 of the OSI model (Network and Transport Layers respectively), examining all incoming and outgoing traffic and blocking those that do not meet specified security rules (ACL). These rules can be based on various criteria such as :

1. IP addresses
2. domain names
3. protocols
4. programs
5. ports
6. keywords etc.

## Types of Firewalls:

Firewalls can be categorized as Hardware, Software, or a combination of both, each serving the same purpose but with different functionalities.

### Hardware Firewall:

A **Hardware Firewall** can be a dedicated stand-alone hardware device or part of a router, physically positioned between a computer network and a gateway (e.g., a broadband router). It filters network traffic using packet filtering techniques and is often used for large business networks.

### Software Firewall:

A **Software Firewall** is a program installed on a computer, functioning similarly to regular software. It filters traffic for individual home users or small businesses, operating only on the computer on which it's installed, not for the entire network.

Besides, there are many other types of firewalls depending on their features and the level of security they provide. The following are types of firewall techniques that can be implemented as software or hardware:

- **Packet-filtering Firewalls**
- **Circuit-level Gateways**
- **Application-level Gateways (Proxy Firewalls)**
- **Stateful Multi-layer Inspection (SMLI) Firewalls**
- **Next-generation Firewalls (NGFW)**
- **Threat-focused NGFW**
- **Network Address Translation (NAT) Firewalls**
- **Cloud Firewalls**
- **Unified Threat Management (UTM) Firewalls**

It's often recommended to have both Hardware and Software Firewalls for optimal protection.

## Conclusion

firewalls are indispensable components of network security infrastructure, serving as the primary defense against unauthorized access, cyber threats, and data breaches. By monitoring and controlling the flow of network traffic based on predefined rules, firewalls act as a barrier between trusted internal networks and untrusted external networks, such as the internet.

## References

- [geeksforgeeks](https://www.geeksforgeeks.org/introduction-of-firewall-in-computer-network/)

- [mdn web docs](https://developer.mozilla.org/en-US/docs/Glossary/firewall)
- [wikipedia](<https://en.wikipedia.org/wiki/Firewall_(computing)>)
