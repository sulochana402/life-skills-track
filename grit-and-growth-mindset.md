# Grit and Growth Mindset

## 1. Grit 

Grit means not giving up, even when things get really tough. It's like having a super strong willpower that helps you keep pushing forward towards your goals, no matter what challenges come your way.

## 2. Growth

A growth mindset means believing that you can get better at things with practice and effort. It's like thinking, "I can learn and improve if I keep trying," instead of believing that your abilities are fixed.

## 3. What is the Internal Locus of Control? 

Internal locus of control is like believing that you hold the steering wheel of your life. You feel that your decisions and actions can shape your destiny, rather than leaving it up to chance or other people's influence.

## 4. What are the key points mentioned by speaker to build growth mindset?


Here are the key points typically mentioned by a speaker when discussing how to build a growth mindset:

- Set learning goals
- Value effort and process
- Learn from failure
- Surround yourself with growth-minded individuals.
- Develop self-reflection
- Seek out feedback


## 5. What are your ideas to take action and build Growth Mindset?

Building a growth mindset involves adopting certain habits and approaches to learning and challenges. Here are some actionable ideas to help you develop a growth mindset:

- **Challenge Yourself:** Seek out tasks that stretch your abilities and embrace them as opportunities for growth.

- **Maintain Optimism:** Cultivate a positive outlook, believing in your ability to overcome challenges and achieve your goals through perseverance and determination.

- **Learn from Setbacks:** Instead of dwelling on failures, analyze them to uncover lessons and insights that can guide your future efforts.

- **Set Learning Goals:** Establish clear goals that focus on acquiring new skills and knowledge, and break them down into manageable steps.