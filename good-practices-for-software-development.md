# Good Practices for Software Development

## 1. Which point(s) were new to you?

- **Over-communication:** Ensure that everyone is aware of changes, issues, and progress by using group channels, attending meetings with video enabled, and promptly responding to messages and calls.

- **Ask Questions Effectively:** Clearly articulate problems, outline any attempted solutions, and provide context or visuals, such as code snippets, to facilitate understanding when seeking assistance or clarification.

- **Be Mindful of Others:** Respect others' communication preferences and time constraints by using appropriate mediums for communication, consolidating messages where possible to avoid clutter, and being responsive to maintain smooth and efficient conversations.


## 2. Which area do you think you need to improve on? What are your ideas to make progress in that area?

I need to get better at asking questions. To do that, I will make sure I explain my problems clearly and mention what I've tried. I will also use pictures or diagrams to help explain things. And when sharing code or project details, I will use tools like Loom or GitHub gists to make it easier for others to understand.