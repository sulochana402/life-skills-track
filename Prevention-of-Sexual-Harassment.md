# Types of Behavior that Cause Sexual Harassment

## Sexual Harassment Definition:
Any unwelcome verbal, visual, or physical conduct of a sexual nature that is severe or pervasive and affects working conditions or creates a hostile work environment is considered sexual harassment.

### Three Forms of Sexual Harassment:

1. **Verbal Harassment:**
   - Comments about clothing, a person's body, sexual or gender-based jokes or remarks.
   - Requesting sexual favors or repeatedly asking a person out.
   - Sexual innuendos, threats, spreading rumors about a person's personal or sexual life.
   - Usage of foul and obscene language.

2. **Visual Harassment:**
   - Obscene posters, drawings/pictures, screensavers, cartoons, emails, or text of a sexual nature.

3. **Physical Harassment:**
   - Sexual assault, impeding or blocking movement.
   - Inappropriate touching such as kissing, hugging, patting, stroking, or rubbing.
   - Sexual gesturing, leering, or staring.

## Two Categories of Sexual Harassment:

1. **Quid Pro Quo:**
   - Occurs when an employer or supervisor uses job rewards or punishments to coerce an employee into a sexual relationship or sexual act.
   - Includes offering the employee raises or promotions, or threatening demotions, firing, or other penalties for not complying with the request.

2. **Hostile Work Environment:**
   - Any conduct directed at an employee because of their sex that unreasonably interferes with the employee’s work performance or creates an intimidating, hostile, or offensive working environment.
   - Usually happens when someone makes repeated sexual comments, making another employee feel so uncomfortable that their work performance suffers.

## I will do the following things If I face or witness sexual harassment:
   
   - I try to say harasser it is not a correct way.
   - I try to distract the harasser by interrupting them. I try to deviate the topic.

   - If the harasser won't listen, I ask for someone else to help.

   - Inform the harasser that their behavior is inappropriate.

   - I keep all the records related to the harassment
   - I report the harassment issue to the assigned authorities.

   - Report the harassment issue to the appropriate authorities or HR department.

  
